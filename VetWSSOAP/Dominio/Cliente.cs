﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace VetWSSOAP.Dominio
{
    [DataContract]
    public class Cliente
    {
        [DataMember]
        public int IDCliente { get; set; }
        [DataMember]
        public string Nombres { get; set; }
        [DataMember]
        public string ApellidoPat { get; set; }
        [DataMember]
        public string ApellidoMat { get; set; }
        [DataMember]
        public string TelefonoFijo { get; set; }
        [DataMember]
        public string TelefonoMovil { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public int IDTipoDocumento { get; set; }
        [DataMember]
        public string Documento { get; set; }
        [DataMember]
        public DateTime FechaNacimiento { get; set; }
        [DataMember]
        public int IDUsuario { get; set; }

    }
}
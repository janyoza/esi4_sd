﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace VetWSSOAP.Dominio
{
    [DataContract]
    public class Mascota
    {

        [DataMember]
        public int IDMascota { get; set; }
        [DataMember]
        public int IDCliente { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public DateTime FechaNacimiento { get; set; }
        [DataMember]
        public int IDTipoMascota { get; set; }
        [DataMember]
        public int IDRazaMascota { get; set; }
        [DataMember]
        public string Observaciones { get; set; }


    }
}
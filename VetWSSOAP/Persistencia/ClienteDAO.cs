﻿using VetWSSOAP.Dominio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;




namespace VetWSSOAP.Persistencia
{
    public class ClienteDAO
    {
        private string CadenaConexion = "Data Source=(local); Initial Catalog=VetDSD; Integrated Security=SSPI";

        public Cliente Crear(Cliente clienteACrear)
        {
            Cliente ClienteReg = null;
            Int32 ID = 0;

            string sql1 = "insert into cliente(nombres, apellidoPat, apellidoMat, " +
                "telefonofijo, telefonoMovil, correo, direccion, idTipoDocumento, documento, " +
                "idUsuario, fechaNacimiento) " +
                "values (@nombres, @apellidoPat, @apellidoMat, @telefonofijo, @telefonoMovil, " +
                "@correo, @direccion, @idTipoDocumento, @documento, " +
                "@idusuario, @fechaNacimiento); " +
                "select cast(scope_identity() as int)";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql1, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@nombres", clienteACrear.Nombres));
                    comando.Parameters.Add(new SqlParameter("@apellidoPat", clienteACrear.ApellidoPat));
                    comando.Parameters.Add(new SqlParameter("@apellidoMat", clienteACrear.ApellidoMat));
                    comando.Parameters.Add(new SqlParameter("@telefonofijo", clienteACrear.TelefonoFijo));
                    comando.Parameters.Add(new SqlParameter("@telefonoMovil", clienteACrear.TelefonoMovil));
                    comando.Parameters.Add(new SqlParameter("@correo", clienteACrear.Correo));
                    comando.Parameters.Add(new SqlParameter("@direccion", clienteACrear.Direccion));
                    comando.Parameters.Add(new SqlParameter("@idTipoDocumento", clienteACrear.IDTipoDocumento));
                    comando.Parameters.Add(new SqlParameter("@documento", clienteACrear.Documento));
                    comando.Parameters.Add(new SqlParameter("@fechaNacimiento", clienteACrear.FechaNacimiento));
                    comando.Parameters.Add(new SqlParameter("@idusuario", clienteACrear.IDUsuario));

                    ID = (Int32)comando.ExecuteScalar();
                }
                ClienteReg = Obtener(ID);
                return ClienteReg;
            }

        }


        public Cliente Obtener(int IDCliente)
        {
            Cliente clienteEncontrado = null;
            string sql = "select * from cliente where IDCliente = @IDCliente";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@IDCliente", IDCliente));

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            clienteEncontrado = new Cliente()
                            {
                                IDCliente = (int)resultado["IDCliente"],
                                Nombres = (string)resultado["nombres"],
                                ApellidoPat = (string)resultado["apellidoPat"],
                                ApellidoMat = (string)resultado["apellidoMat"],
                                TelefonoFijo = (string)resultado["telefonofijo"],
                                Correo = (string)resultado["correo"],
                                Direccion = (string)resultado["direccion"],
                                IDTipoDocumento = (int)resultado["idTipoDocumento"],
                                Documento = (string)resultado["documento"],
                                FechaNacimiento = (DateTime)resultado["fechaNacimiento"],
                                IDUsuario = (int)resultado["idusuario"]
                            };
                        }
                    }
                }
                return clienteEncontrado;
            }
        }
 
                
                
        public Cliente Modificar(Cliente clienteAModificar)
        {

            Cliente ClienteReg = null;

            string sql = "update cliente set nombres= @nombres, apellidoPat= @apellidoPat, apellidoMat=@apellidoMat," +
                "telefonofijo=@telefonofijo, telefonomovil=@telefonomovil, correo=@correo, direccion=@direccion, idTipoDocumento=@idTipoDocumento," +
                "documento=@documento, fechaNacimiento= @fechaNacimiento, idUsuario=@idusuario " +
                "where IDCliente=@IDCliente";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@nombres", clienteAModificar.Nombres));
                    comando.Parameters.Add(new SqlParameter("@apellidoPat", clienteAModificar.ApellidoPat));
                    comando.Parameters.Add(new SqlParameter("@apellidoMat", clienteAModificar.ApellidoMat));
                    comando.Parameters.Add(new SqlParameter("@telefonofijo", clienteAModificar.TelefonoFijo));
                    comando.Parameters.Add(new SqlParameter("@telefonomovil", clienteAModificar.TelefonoMovil));
                    comando.Parameters.Add(new SqlParameter("@correo", clienteAModificar.Correo));
                    comando.Parameters.Add(new SqlParameter("@direccion", clienteAModificar.Direccion));
                    comando.Parameters.Add(new SqlParameter("@idTipoDocumento", clienteAModificar.IDTipoDocumento));
                    comando.Parameters.Add(new SqlParameter("@documento", clienteAModificar.Documento));
                    comando.Parameters.Add(new SqlParameter("@fechaNacimiento", clienteAModificar.FechaNacimiento));
                    comando.Parameters.Add(new SqlParameter("@idusuario", clienteAModificar.IDUsuario));

                    comando.Parameters.Add(new SqlParameter("@IDCliente", clienteAModificar.IDCliente));

                    comando.ExecuteNonQuery();

                }
                ClienteReg = Obtener(clienteAModificar.IDCliente);
                return ClienteReg;
            }

        }



        public void Eliminar(int IDCliente)
        {
            string sql = "delete from cliente where IDCliente=@IDCliente";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@IDCliente", IDCliente));
                    comando.ExecuteNonQuery();
                }
            }

        }



        public List<Cliente> Listar()
        {
            List<Cliente> clientesEncontrados = new List<Cliente>();
            Cliente clienteEncontrado = null;


            string sql = "select * from cliente";

            using (SqlConnection conexion = new SqlConnection(CadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {

                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            clienteEncontrado = new Cliente()
                            {
                                IDCliente = (int)resultado["IDCliente"],
                                Nombres = (string)resultado["nombres"],
                                ApellidoPat = (string)resultado["apellidoPat"],
                                ApellidoMat = (string)resultado["apellidoMat"],
                                TelefonoFijo = (string)resultado["telefonofijo"],
                                Correo = (string)resultado["correo"],
                                Direccion = (string)resultado["direccion"],
                                IDTipoDocumento = (int)resultado["idTipoDocumento"],
                                Documento = (string)resultado["documento"],
                                FechaNacimiento = (DateTime)resultado["fechaNacimiento"],
                                IDUsuario = (int)resultado["idusuario"]
                            };

                            clientesEncontrados.Add(clienteEncontrado);
                        }
                    }
                }
            }
            return clientesEncontrados;

        }
    }
}
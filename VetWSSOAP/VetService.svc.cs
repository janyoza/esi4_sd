﻿using VetWSSOAP.Dominio;
using VetWSSOAP.Errores;
using VetWSSOAP.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace VetWSSOAP
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "VetService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione VetService.svc o VetService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class VetService : IVetService
    {

        private ClienteDAO clienteDAO = new ClienteDAO();
        private MascotaDAO mascotaDAO = new MascotaDAO();

        public Cliente CrearCliente(Cliente clienteACrear)
        {
            return clienteDAO.Crear(clienteACrear);
        }

        public Mascota CrearMascota(Mascota mascotaACrear)
        {
            return mascotaDAO.Crear(mascotaACrear);
        }

        public void EliminarCliente(int IDCliente)
        {
            clienteDAO.Eliminar(IDCliente);
        }

        public void EliminarMascota(int IDMascota)
        {
            mascotaDAO.Eliminar(IDMascota);
        }

        public List<Cliente> ListarClientes()
        {
            return clienteDAO.Listar();
        }

        public List<Mascota> ListarMascotas()
        {
            return mascotaDAO.Listar();
        }

        public Cliente ModificarCliente(Cliente clienteAModificar)
        {
            return clienteDAO.Modificar(clienteAModificar);

        }

        public Mascota ModificarMascota(Mascota mascotaAModificar)
        {
            return mascotaDAO.Modificar(mascotaAModificar);
        }

        public Cliente ObtenerCliente(int IDCliente)
        {
            return clienteDAO.Obtener(IDCliente);
        }

        public Mascota ObtenerMascota(int IDMascota)
        {
            return mascotaDAO.Obtener(IDMascota);
        }
    }
}
